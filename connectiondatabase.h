#ifndef CONNECTIONDATABASE_H
#define CONNECTIONDATABASE_H
#include "arrow.h"
#include"vector"

#include <unordered_map>

class ConnectionDatabase
{
public:
	static ConnectionDatabase *instance();
	// bunlari duzelt
	DiagramItem *startItem = nullptr, *endItem = nullptr;
//	Arrow *createArrow;
	void addItem(DiagramItem *item) {itemList.push_back(item);}
	std::vector <DiagramItem *>  getItemList() {return itemList;}
protected:
	ConnectionDatabase();

	std::vector <DiagramItem *> itemList;
	std::unordered_map<std::string, std::string> connectionList;
};

#endif // CONNECTIONDATABASE_H
